# Запуск в docker
- `docker-compose -f ./deploy/docker/docker-compose.yml -p crud up`
Пример запроса `localhost:8080/hello/template`

# Запуск в minikube
- `kubectl create -f ./deploy/k8s`
- добавить в hosts запись <ip миникуба>:minikube 
Пример запроса `http://minikube/crud/hello/template`


# To use local images in minikube:
eval $(minikube docker-env)
и в  deployment - imagePullPolicy: IfNotPresent