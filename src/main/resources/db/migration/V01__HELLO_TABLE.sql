CREATE SEQUENCE hello_id_seq
  START WITH 1
  INCREMENT BY 1;

CREATE TABLE hello (
  id   BIGINT PRIMARY KEY default nextval('hello_id_seq'),
  name VARCHAR
);

insert into hello (name)
values ('one'),
       ('two'),
       ('three'),
       ('four'),
       ('five');
