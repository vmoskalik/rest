package ru.sberned.crud.config.property

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("bean")
class MyProperties {

    lateinit var message: String
}