package ru.sberned.crud.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.transaction.annotation.EnableTransactionManagement
import ru.sberned.crud.entity.HelloEntity
import ru.sberned.crud.repository.HelloRepository

@Configuration
@EnableScheduling
@EnableJpaRepositories(basePackageClasses = [HelloRepository::class])
@EnableTransactionManagement
@EntityScan(basePackageClasses = [HelloEntity::class, Jsr310JpaConverters::class])
class Config