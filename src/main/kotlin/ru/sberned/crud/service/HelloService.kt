package ru.sberned.crud.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.sberned.crud.repository.HelloRepository
import ru.sberned.crud.web.converter.HelloConverter.Companion.toDTO
import ru.sberned.crud.web.converter.HelloConverter.Companion.toEntity
import ru.sberned.crud.web.dto.HelloDTO

@Service
@Transactional
class HelloService(
        private val helloRepository: HelloRepository
) {
    fun findOne(id: Long): HelloDTO? = helloRepository.findById(id)
            .map { toDTO(it) }
            .orElse(null)

    fun findAll(): List<HelloDTO> = helloRepository.findAll().map { toDTO(it) }

    fun save(dto: HelloDTO): HelloDTO {
        val entity = helloRepository.save(toEntity(dto))
        return toDTO(entity)
    }

    fun delete(id: Long) = helloRepository.deleteById(id)
}