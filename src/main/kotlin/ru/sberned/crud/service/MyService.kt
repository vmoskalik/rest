package ru.sberned.crud.service

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.sberned.crud.config.property.MyProperties

@Service
class MyService(
        private val myProperties: MyProperties
) {

    @Scheduled(fixedDelay = 5000)
    fun test() {
        println(myProperties.message)
    }
}