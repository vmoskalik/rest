package ru.sberned.crud.entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Parameter
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "hello")
class HelloEntity(
        @get:NotBlank
        var name: String
) {
    @Id
    @GenericGenerator(
            name = "helloGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = [
                Parameter(name = "sequence_name", value = "hello_id_seq"),
                Parameter(name = "initial_value", value = "1"),
                Parameter(name = "increment_size", value = "1")
            ]
    )
    @GeneratedValue(generator = "helloGenerator")
    var id: Long? = null
}