package ru.sberned.crud.web

import org.springframework.web.bind.annotation.*
import ru.sberned.crud.service.HelloService
import ru.sberned.crud.web.dto.HelloDTO
import ru.sberned.crud.web.dto.TemplateHelloDTO
import javax.validation.Valid

@RestController
@RequestMapping("/hello")
class HelloController(
        private val helloService: HelloService
) {
    @GetMapping("/template")
    fun template(): TemplateHelloDTO = TemplateHelloDTO()

    @GetMapping
    fun findAll() = helloService.findAll()

    @GetMapping("/{id}")
    fun findOne(@PathVariable(name = "id") id: Long) = helloService.findOne(id)

    @PostMapping
    fun save(@Valid @RequestBody dto: HelloDTO): HelloDTO {
        if (dto.id != null) {
            throw RuntimeException("id must be null")
        }
        return helloService.save(dto)
    }

    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: Long,
               @Valid @RequestBody dto: HelloDTO): HelloDTO {
        if (id != dto.id) {
            throw RuntimeException("id=$id not equal to dto.id=${dto.id}")
        }
        helloService.findOne(id) ?: throw RuntimeException("HelloEntity with id=$id not found")
        return helloService.save(dto)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable(name = "id") id: Long) = helloService.delete(id)
}