package ru.sberned.crud.web.converter

import ru.sberned.crud.entity.HelloEntity
import ru.sberned.crud.web.dto.HelloDTO

class HelloConverter {
    companion object {
        fun toEntity(dto: HelloDTO): HelloEntity {
            return HelloEntity(dto.name).apply { id = dto.id }
        }

        fun toDTO(entity: HelloEntity): HelloDTO {
            return HelloDTO(entity.id, entity.name)
        }
    }
}