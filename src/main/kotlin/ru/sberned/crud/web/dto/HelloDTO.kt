package ru.sberned.crud.web.dto

import javax.validation.constraints.NotBlank

data class HelloDTO(
        var id: Long?,

        @get:NotBlank
        var name: String
)