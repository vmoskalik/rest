package ru.sberned.crud.web.dto

data class TemplateHelloDTO(
        val id: Long = 0
) {
    val name: String = "Hello, World!"
}