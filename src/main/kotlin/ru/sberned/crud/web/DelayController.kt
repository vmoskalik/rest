package ru.sberned.crud.web

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.sberned.crud.service.HelloService

@RestController
@RequestMapping("/delay")
class DelayController(
        private val helloService: HelloService
) {
    @GetMapping("/one")
    fun one() {
        Thread.sleep(1000)
    }

    @GetMapping("/two")
    fun two() {
        Thread.sleep(2000)
    }

    @GetMapping("/three")
    fun three() {
        Thread.sleep(3000)
    }
}