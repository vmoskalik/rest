package ru.sberned.crud.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.sberned.crud.entity.HelloEntity

@Repository
interface HelloRepository : JpaRepository<HelloEntity, Long>
